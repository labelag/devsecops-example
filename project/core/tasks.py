from celery import shared_task
from celery.utils.log import get_task_logger
from huawei_lte_api.Client import Client
from huawei_lte_api.Connection import Connection
from django.conf import settings

logger = get_task_logger(__name__)
import requests

@shared_task
def sample_task():
    mylist = []
    connection = Connection('http://{}:{}@192.168.8.1/'.format(settings.ROUTERUSER,settings.ROUTERPASSWORD))
    client = Client(connection)
    payload = {"email": settings.BAZARLI_USER, "password": settings.BAZARLI_PASSWORD}
    url = "{}/api/admin/login".format(settings.BAZARLI_HOST)
    myr = requests.post(url=url, json=payload)
    jsonobject = myr.json()
    if jsonobject.get('token') is not None:
        token = myr.json()['token']
        headers = {"Authorization": "Bearer {}".format(token)}
    msg = client.sms.get_sms_list()
    if(msg['Count'] !='0'):
        if (msg['Count'] > '1'):
            mylist = msg['Messages']['Message']
        else:
            mylist.append(msg['Messages']['Message'])
        for s in mylist:
            if (headers is not None):
                res = requests.post(url="{}/api/storeOtpMessages".format(settings.BAZARLI_HOST),
                                    json={"message_text": s['Content'], "phone_no": s['Phone']}, headers=headers)
                if (res.status_code == 200):
                    logger.info("SMS sent succesfully {}".format(s))
                    client.sms.delete_sms(s['Index'])
    client.user.logout()
    return True

